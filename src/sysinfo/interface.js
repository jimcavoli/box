'use strict';

// -------------------------------------------
//  This file just describes the interface
//
//  New backends can start from here
// -------------------------------------------

exports = module.exports = {
    getServerIp,
    testConfig
};

var assert = require('assert'),
    BoxError = require('../boxerror.js');

function getServerIp(config, callback) {
    assert.strictEqual(typeof config, 'object');
    assert.strictEqual(typeof callback, 'function');

    callback(new BoxError(BoxError.NOT_IMPLEMENTED, 'testConfig is not implemented'));
}

function testConfig(config, callback) {
    assert.strictEqual(typeof config, 'object');
    assert.strictEqual(typeof callback, 'function');

    callback(null);
}
