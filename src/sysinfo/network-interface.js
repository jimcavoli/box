'use strict';

exports = module.exports = {
    getServerIp,
    testConfig
};

var assert = require('assert'),
    BoxError = require('../boxerror.js'),
    debug = require('debug')('box:sysinfo/network-interface'),
    os = require('os');

function getServerIp(config, callback) {
    assert.strictEqual(typeof config, 'object');
    assert.strictEqual(typeof callback, 'function');

    const ifaces = os.networkInterfaces();
    const iface = ifaces[config.ifname]; // array of addresses
    if (!iface) return callback(new BoxError(BoxError.NETWORK_ERROR, `No interface named ${config.ifname}`));

    const addresses = iface.filter(i => i.family === 'IPv4').map(i => i.address);
    if (addresses.length === 0) return callback(new BoxError(BoxError.NETWORK_ERROR, `${config.ifname} does not have any IPv4 address`));
    if (addresses.length > 1) debug(`${config.ifname} has multiple ipv4 - ${JSON.stringify(addresses)}. choosing the first one.`);

    return callback(null, addresses[0]);
}

function testConfig(config, callback) {
    assert.strictEqual(typeof config, 'object');
    assert.strictEqual(typeof callback, 'function');

    if (typeof config.ifname !== 'string') return callback(new BoxError(BoxError.BAD_FIELD, 'ifname is not a string'));

    getServerIp(config, function (error) {
        if (error) return callback(error);

        callback(null);
    });
}
