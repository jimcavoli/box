'use strict';

exports = module.exports = {
    getRemoteSupport: getRemoteSupport,
    enableRemoteSupport: enableRemoteSupport
};

let assert = require('assert'),
    BoxError = require('./boxerror.js'),
    constants = require('./constants.js'),
    eventlog = require('./eventlog.js'),
    once = require('once'),
    path = require('path'),
    paths = require('./paths.js'),
    settings = require('./settings.js'),
    shell = require('./shell.js');

// the logic here is also used in the cloudron-support tool
const AUTHORIZED_KEYS_CMD = path.join(__dirname, 'scripts/remotesupport.sh');

function sshInfo() {
    let filePath, user;

    if (constants.TEST) {
        filePath = path.join(paths.baseDir(), 'authorized_keys');
        user = process.getuid();
    } else if (settings.provider() === 'ec2' || settings.provider() === 'lightsail' || settings.provider() === 'ami') {
        filePath = '/home/ubuntu/.ssh/authorized_keys';
        user = 'ubuntu';
    } else {
        filePath = '/root/.ssh/authorized_keys';
        user = 'root';
    }

    return { filePath, user };
}

function getRemoteSupport(callback) {
    assert.strictEqual(typeof callback, 'function');

    callback = once(callback); // exit may or may not be called after an 'error'

    let result = '';
    let cp = shell.sudo('support', [ AUTHORIZED_KEYS_CMD, 'is-enabled', sshInfo().filePath ], {}, function (error) {
        if (error) callback(new BoxError(BoxError.FS_ERROR, error));

        callback(null, { enabled: result.trim() === 'true' });
    });
    cp.stdout.on('data', (data) => result = result + data.toString('utf8'));
}

function enableRemoteSupport(enable, auditSource, callback) {
    assert.strictEqual(typeof enable, 'boolean');
    assert.strictEqual(typeof auditSource, 'object');
    assert.strictEqual(typeof callback, 'function');

    let si = sshInfo();
    shell.sudo('support', [ AUTHORIZED_KEYS_CMD, enable ? 'enable' : 'disable', si.filePath, si.user ], {}, function (error) {
        if (error) callback(new BoxError(BoxError.FS_ERROR, error));

        eventlog.add(eventlog.ACTION_SUPPORT_SSH, auditSource, { enable });

        callback();
    });
}
