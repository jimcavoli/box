'use strict';

exports = module.exports = {
    getApps: getApps,
    getApp: getApp,
    getAppVersion: getAppVersion,

    createUserToken: createUserToken,
    registerCloudron: registerCloudron,
    getSubscription: getSubscription
};

var appstore = require('../appstore.js'),
    assert = require('assert'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess;

function getApps(req, res, next) {
    appstore.getApps(function (error, apps) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { apps }));
    });
}

function getApp(req, res, next) {
    assert.strictEqual(typeof req.params.appstoreId, 'string');

    appstore.getApp(req.params.appstoreId, function (error, app) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, app));
    });
}

function getAppVersion(req, res, next) {
    assert.strictEqual(typeof req.params.appstoreId, 'string');
    assert.strictEqual(typeof req.params.versionId, 'string');

    appstore.getAppVersion(req.params.appstoreId, req.params.versionId, function (error, manifest) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, manifest));
    });
}

function createUserToken(req, res, next) {
    appstore.getUserToken(function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, { accessToken: result }));
    });
}

function registerCloudron(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.email !== 'string' || !req.body.email) return next(new HttpError(400, 'email must be string'));
    if (typeof req.body.password !== 'string' || !req.body.password) return next(new HttpError(400, 'password must be string'));
    if ('totpToken' in req.body && typeof req.body.totpToken !== 'string') return next(new HttpError(400, 'totpToken must be string'));
    if (typeof req.body.signup !== 'boolean') return next(new HttpError(400, 'signup must be a boolean'));
    if ('purpose' in req.body && typeof req.body.purpose !== 'string') return next(new HttpError(400, 'purpose must be string'));

    appstore.registerWithLoginCredentials(req.body, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, {}));
    });
}

function getSubscription(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    appstore.getSubscription(function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, result)); // { email, cloudronId, cloudronCreatedAt, plan, current_period_end, canceled_at, cancel_at, status, features }
    });
}
