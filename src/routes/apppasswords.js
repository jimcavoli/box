'use strict';

exports = module.exports = {
    list: list,
    get: get,
    del: del,
    add: add
};

var assert = require('assert'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    users = require('../users.js');

function get(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.params.id, 'string');

    users.getAppPassword(req.params.id, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, result));
    });
}

function add(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be string'));
    if (typeof req.body.identifier !== 'string') return next(new HttpError(400, 'identifier must be string'));

    users.addAppPassword(req.user.id, req.body.identifier, req.body.name, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, result));
    });
}

function list(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    users.getAppPasswords(req.user.id, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { appPasswords: result }));
    });
}

function del(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.params.id, 'string');

    // TODO: verify userId owns the id ?
    users.delAppPassword(req.params.id, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204, {}));
    });
}
