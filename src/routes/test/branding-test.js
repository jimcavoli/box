'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

var async = require('async'),
    constants = require('../../constants.js'),
    database = require('../../database.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    paths = require('../../paths.js'),
    server = require('../../server.js'),
    superagent = require('superagent');

var SERVER_URL = 'http://localhost:' + constants.PORT;

var USERNAME = 'superadmin', PASSWORD = 'Foobar?1337', EMAIL ='silly@me.com';
var token = null;

function setup(done) {
    async.series([
        server.start.bind(null),
        database._clear.bind(null),

        function createAdmin(callback) {
            superagent.post(SERVER_URL + '/api/v1/cloudron/activate')
                .query({ setupToken: 'somesetuptoken' })
                .send({ username: USERNAME, password: PASSWORD, email: EMAIL })
                .end(function (error, result) {
                    expect(result).to.be.ok();
                    expect(result.statusCode).to.eql(201);

                    // stash token for further use
                    token = result.body.token;

                    callback();
                });
        }
    ], done);
}

function cleanup(done) {
    database._clear(function (error) {
        expect(!error).to.be.ok();

        server.stop(done);
    });
}

describe('Branding API', function () {
    before(setup);
    after(cleanup);

    describe('cloudron_name', function () {
        var name = 'foobar';

        it('get default succeeds', function (done) {
            superagent.get(SERVER_URL + '/api/v1/branding/cloudron_name')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.name).to.be.ok();
                    done();
                });
        });

        it('cannot set without name', function (done) {
            superagent.post(SERVER_URL + '/api/v1/branding/cloudron_name')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set empty name', function (done) {
            superagent.post(SERVER_URL + '/api/v1/branding/cloudron_name')
                .query({ access_token: token })
                .send({ name: '' })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('set succeeds', function (done) {
            superagent.post(SERVER_URL + '/api/v1/branding/cloudron_name')
                .query({ access_token: token })
                .send({ name: name })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(202);
                    done();
                });
        });

        it('get succeeds', function (done) {
            superagent.get(SERVER_URL + '/api/v1/branding/cloudron_name')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.name).to.eql(name);
                    done();
                });
        });
    });

    describe('cloudron_avatar', function () {
        it('get default succeeds', function (done) {
            superagent.get(SERVER_URL + '/api/v1/branding/cloudron_avatar')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body).to.be.a(Buffer);
                    done();
                });
        });

        it('cannot set without data', function (done) {
            superagent.post(SERVER_URL + '/api/v1/branding/cloudron_avatar')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('set succeeds', function (done) {
            superagent.post(SERVER_URL + '/api/v1/branding/cloudron_avatar')
                .query({ access_token: token })
                .attach('avatar', paths.CLOUDRON_DEFAULT_AVATAR_FILE)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(202);
                    done();
                });
        });

        it('get succeeds', function (done) {
            superagent.get(SERVER_URL + '/api/v1/branding/cloudron_avatar')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.toString()).to.eql(fs.readFileSync(paths.CLOUDRON_DEFAULT_AVATAR_FILE, 'utf-8'));
                    done(err);
                });
        });
    });

    describe('appstore listing config', function () {
        it('get default succeeds', function (done) {
            superagent.get(SERVER_URL + '/api/v1/branding/appstore_listing_config')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.whitelist).to.eql(null);
                    expect(res.body.blacklist).to.eql([]);

                    done();
                });
        });

        it('cannot set with no bl or wl', function (done) {
            superagent.post(SERVER_URL + '/api/v1/branding/appstore_listing_config')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set bad bl', function (done) {
            superagent.post(SERVER_URL + '/api/v1/branding/appstore_listing_config')
                .query({ access_token: token })
                .send({ blacklist: [ 1 ] })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set bad wl', function (done) {
            superagent.post(SERVER_URL + '/api/v1/branding/appstore_listing_config')
                .query({ access_token: token })
                .send({ whitelist: 4 })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('set bl succeeds', function (done) {
            superagent.post(SERVER_URL + '/api/v1/branding/appstore_listing_config')
                .query({ access_token: token })
                .send({ blacklist: [ 'id1', 'id2' ] })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(202);
                    done();
                });
        });

        it('get bl succeeds', function (done) {
            superagent.get(SERVER_URL + '/api/v1/branding/appstore_listing_config')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.blacklist).to.eql([ 'id1', 'id2' ]);
                    expect(res.body.whitelist).to.be(undefined);

                    done();
                });
        });

        it('set wl succeeds', function (done) {
            superagent.post(SERVER_URL + '/api/v1/branding/appstore_listing_config')
                .query({ access_token: token })
                .send({ whitelist: [ 'id1', 'id2' ] })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(202);
                    done();
                });
        });

        it('get wl succeeds', function (done) {
            superagent.get(SERVER_URL + '/api/v1/branding/appstore_listing_config')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.whitelist).to.eql([ 'id1', 'id2' ]);
                    expect(res.body.blacklist).to.be(undefined);

                    done();
                });
        });

    });

});
