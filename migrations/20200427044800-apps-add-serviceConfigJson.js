'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE apps ADD COLUMN serviceConfigJson TEXT', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE apps DROP COLUMN serviceConfigJson', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
