'use strict';

exports.up = function(db, callback) {
    db.runSql('DROP TABLE clients', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    var cmd = `CREATE TABLE IF NOT EXISTS clients(
      id VARCHAR(128) NOT NULL UNIQUE,
      appId VARCHAR(128) NOT NULL,
      type VARCHAR(16) NOT NULL,
      clientSecret VARCHAR(512) NOT NULL,
      redirectURI VARCHAR(512) NOT NULL,
      scope VARCHAR(512) NOT NULL,
      PRIMARY KEY(id)) CHARACTER SET utf8 COLLATE utf8_bin`;

    db.runSql(cmd, function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
