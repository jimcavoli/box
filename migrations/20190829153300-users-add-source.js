'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE users ADD COLUMN source VARCHAR(128) DEFAULT ""', function (error) {
        if (error) return callback(error);

        callback();
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE users DROP COLUMN source', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

