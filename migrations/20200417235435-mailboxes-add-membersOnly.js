'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE mailboxes ADD COLUMN membersOnly BOOLEAN DEFAULT 0', function (error) {
        if (error) return callback(error);

        callback();
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE users DROP COLUMN membersOnly', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

